#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#define ARRSIZE 10

void fill_array_random(int numbers[], int size);
void countElement(int inputArray[], int arraySize, int elementToCount, int *found_elements);  //changed
//==============================================================================
int main(int argc, char** argv)
{
    int numbers[ARRSIZE];
    int i;
    int user;
    int run = 1;
    int found_elements; //added

    srand(time(NULL));

    while(run)
    {
        printf("\n");
        fill_array_random(numbers, ARRSIZE);
        printf("Printing %d random numbers\n", ARRSIZE);
        for(i = 0; i < ARRSIZE; i++)
        {
            printf("Number: %d\n", numbers[i]);
        }

        printf("\nWhat to search for: ");
        scanf("%d", &user);
        countElement(numbers, ARRSIZE, user, &found_elements); //changed
        printf("The number %d occurs %d times.\n", user, found_elements);  //changed

        printf("\nDo you wish to generate a new sequence (1 for yes, 0 for no)?: ");
        scanf("%d", &run);

    }
    return 0;
}
//==============================================================================
void fill_array_random(int numbers[], int size)
{
    int i;

    for (i = 0; i < size; i++)
    {
        numbers[i] = rand()%10+1;
    }
}
//==============================================================================
void countElement(int inputArray[], int arraySize, int elementToCount, int *found_elements)   //changed
{
    int i;
    int counter = 0;

    for(i = 0; i < arraySize; i++)
    {
        if (inputArray[i] == elementToCount)
        {
            counter++;
        }
    }
    *found_elements = counter; //changed
}
