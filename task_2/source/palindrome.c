#include <stdio.h>
#include <string.h>
#include <ctype.h>

//Takes strings and checks if they are palindromes.

#define ARRSIZE 1000

void to_lower_case(char*);
void remove_nonletter(char*);
int isPalindrome(char*);
//==============================================================================
int main(int argc, char** argv)
{
    char user1[ARRSIZE];        //name change
    char user2[ARRSIZE];        //added
    char slask;
    int run = 1;

    while(run)
    {
        //Accepts empty and non letter strings as palindromes.
        printf("\nEnter a string: ");
        gets(user1);                                //changed
        printf("\nEnter a  second string: ");       //added
        gets(user2);

        remove_nonletter(user1);    //changed
        to_lower_case(user1);       //changed

        remove_nonletter(user2);    //added
        to_lower_case(user2);       //added

        if (strcmp(user1, user2) == 0)      //added
        {
            printf("\nThe strings are semantically equal.\n");
        }
        else                                //added
        {
            printf("\nThese strings are semantically different from each other.\n");
        }
        /*
           if(isPalindrome(user) == 1)
           {
           printf("Your string is a palindrome\n");
           }
           else
           {
           printf("Your string is not a paindrome\n");
           }
         */
        printf("\nDo you want to enter another string (0 for no, 1 for yes)?: ");
        scanf("%d", &run);
        scanf("%c", &slask);
    }
    return 0;
}
//==============================================================================
void to_lower_case(char inputString[])
{
    int i;
    int length = strlen(inputString) - 1;

    for(i = 0; i <= length; i++)
    {
        inputString[i] = tolower(inputString[i]);
    }
}
//==============================================================================
void remove_nonletter(char inputString[])
{
    int i;
    int j = 0;
    int length = strlen(inputString) - 1;

    for(i = 0; i <= length; i++)
    {
        if(isalpha(inputString[i]))
        {
            inputString[j] = inputString[i];
            j++;
        }
    }
    inputString[j] = '\0';
}
//==============================================================================
int isPalindrome(char inputString[])
{
    //Accepts empty strings as palindromes.
    int i;
    int length = strlen(inputString) - 1;

    for(i = 0; i <= length; i++)
    {
        if(inputString[i] != inputString[length - i])
        {
            return 0;
        }
    }
    return 1;
}
